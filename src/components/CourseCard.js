import {useState} from 'react';
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {

	//Use the state hook  for this component to be able to store its state

	/*
		syntax:
			const [getter, seeter]= useState(initialGetterValue
	*/

	//*********** Activity
	// const [count, setCount] = useState(0)

	// function enroll() {
	// 	if(count <= 29){
	// 	setCount(count + 1)
	// 	console.log('Enrollees' + count)
	// }else{
	// 	return alert("no more seat")
	// }
	// }
	// console.log(props.courseProp)
	const {name, description, price, _id} = courseProp

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
			</Card.Body>
		</Card>

		)

}