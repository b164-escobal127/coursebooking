import { useState, useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';

export default function Login() {

		const {user, setUser} = useContext(UserContext);

		// State hooks to store the values of the input fields
		const [email, setEmail] = useState("");
	    const [password, setPassword] = useState("");
	    // State to determine whether submit button is enabled or not
	    const [isActive, setIsActive] = useState(true);


	function authenticate(e) {

		e.preventDefault()

		fetch('http://localhost:4000/api/users/login', { 
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			//console.log(data)

			if(typeof data.accessToken !== "undefined") {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt"
				});
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		})


		setEmail("");
		setPassword("");

		/*
		Syntax:
			localStorage.setItem("propertyName", value)

		*/
		// localStorage.setItem("email", email)

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// alert(`${email} has been verified. Welcome back!`);
	}

	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/api/users/details', {

			headers: {
				Authorization: `Bearer ${ token }`
			}


		})
			.then(res => res.json())
			.then(data => {

				//console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})

	}


	useEffect(()=> {
		//Validation to enable the submit button when all the input fields are populated and both passwords match 
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(

		(user.id !== null)?

		<Navigate to ="/courses" />

		:


		<Form onSubmit={(e) => authenticate(e)}>
		<h1 className="text-center">Login</h1>
		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email"
		    	value={email}
		    	onChange={e =>setEmail(e.target.value)} 
		    	required
		    />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password" 
		    	value={password}
		    	onChange={e => setPassword(e.target.value)}
		    	required
		    />
		  </Form.Group>
		
		  {
		  	isActive ?
		  	<Button variant="success" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>

		  }
		</Form>

		)
}