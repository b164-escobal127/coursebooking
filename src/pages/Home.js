import { Fragment } from 'react';
import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';


export default function Home() {

	return(
		<Fragment>
			<Banner />
			<Highlights />
			{/*<CourseCard />*/}
		</Fragment>
		)

}