const coursesData = [
	
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quo provident minima dicta cupiditate quos soluta officiis, nihil impedit numquam reiciendis vero, nam doloribus earum aspernatur qui est ipsa nisi quia?",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc002",
		name: "Python-Diego",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quo provident minima dicta cupiditate quos soluta officiis, nihil impedit numquam reiciendis vero, nam doloribus earum aspernatur qui est ipsa nisi quia?",
		price: 50000,
		onOffer: true
	},

	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quo provident minima dicta cupiditate quos soluta officiis, nihil impedit numquam reiciendis vero, nam doloribus earum aspernatur qui est ipsa nisi quia?",
		price: 55000,
		onOffer: true
	}





]

export default coursesData;